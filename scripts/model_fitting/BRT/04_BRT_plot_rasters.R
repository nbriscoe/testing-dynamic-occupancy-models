######################################################
## Basic raster plot with occ data overlayed    ######
######################################################

#Plot predictions for visual checking

load("data/occdyn_vars_AUG.RData") #Load species & env data

library(raster)

#species ids as numbers
species <- unique(dets_covs_SDMs_NAs$specid)

#species ids as names
spnames <- dets_covs_SDMs_NAs$CommonName[match(species, dets_covs_SDMs_NAs$specid)]

#spnames is a vector of the species collapsed names, ordered from low to high species id numbers
years <- 2000:2013
trainyears <- c("Data0002", "Data0004", "Data0009")


# for(i in 1:length(spnames)){
#   dir.create(file.path("maps",spnames[i]))
# }

for(yy in 1:length(trainyears))
{
  for (sp in 1:length(spnames))  
  {
    thissp <- species[sp]
    spX.dir<-file.path(paste0("July2020/results/BRT/",trainyears[[yy]],"/",spnames[sp]))
    occ.r.files<-list.files(spX.dir, pattern=paste0(spnames[sp], "_collDet_BRT"), full=TRUE)
    occ.st<-stack(occ.r.files)
    crs(occ.st)<-crs(sd_ndvi.st)
    
    #read in observations and add to plot. We omit sites with no data and always plot the collapsed detections per year. 
    moddat.thissp <- dets_covs_SDMs[dets_covs_SDMs$specid==thissp,]  
    #get the collDet locations and other relevant columns
    moddat <- moddat.thissp[,-c(7,9:22)] 
    moddat <- na.omit(moddat)
    coordinates(moddat)<-~coordx + coordy
    crs(moddat)<-crs(sd_ndvi.st)
    
    
    ##Plot
    fileN<-paste0(spX.dir, "/Plots_",trainyears[[yy]],"_",spnames[sp],"_collDet_LandscapePreds.pdf")
    pdf(file=fileN, width=11,height=10)
    
    par(mfrow=c(4,4),mai=c(0.1,0.1,0.1,0.1))
    
    #Set the colour ramp 
    breaks<-seq(0,1,length.out=100)
    cols<-rev(terrain.colors(99))
    labs<-seq(0,1,by=0.1)
    
    for(i in 1:14){
      plot(occ.st[[i]],axes=FALSE, box=FALSE, legend=FALSE, main=years[i], breaks=breaks,col=cols)
      points(moddat[moddat$year==years[i] & moddat$collDet==1,],pch=20, cex=0.7)
    }
    
    #plot 2 "white" maps - only way I could find to move legend over!   
    plot(occ.st[[14]],axes=FALSE, box=FALSE, legend=FALSE, main=NULL, breaks=breaks,col=rep("white",99))
    plot(occ.st[[14]],axes=FALSE, box=FALSE, legend=FALSE, main=NULL, breaks=breaks,col=rep("white",99))
    
    #plot legend in position
    par(mar = c(0.2,0.2,0.2,7))
    plot(occ.st[[14]], legend.only=TRUE, col=cols,breaks = breaks,legend.width=1, legend.shrink=0.75,
         axis.args=list(at=seq(0,1,0.25),
                        labels=seq(0,1,0.25),
                        cex.axis=0.8),
         legend.args=list(text='Prob.Occ', side=3,font=2, line=0.5,cex=0.8))
    
    dev.off()
  }
}



###### now for det2 - still plot the collDet locations over top

# for(i in 1:length(spnames)){
#   dir.create(file.path("maps",spnames[i]))
# }

for(yy in 1:length(trainyears))
{
  for (sp in 1:length(spnames))  
  {
    thissp <- species[sp]
    spX.dir<-file.path(paste0("July2020/results/BRT/",trainyears[[yy]],"/",spnames[sp]))
    occ.r.files<-list.files(spX.dir, pattern=paste0(spnames[sp], "_det2_BRT"), full=TRUE)
    occ.st<-stack(occ.r.files)
    crs(occ.st)<-crs(sd_ndvi.st)
    
    #read in observations and add to plot. We omit sites with no data and always plot the collapsed detections per year. 
    moddat.thissp <- dets_covs_SDMs[dets_covs_SDMs$specid==thissp,]  
    #get the collDet locations and other relevant columns
    moddat <- moddat.thissp[,-c(7,9:22)] 
    moddat <- na.omit(moddat)
    coordinates(moddat)<-~coordx + coordy
    crs(moddat)<-crs(sd_ndvi.st)
    
    
    ##Plot
    fileN<-paste0(spX.dir, "/Plots_",trainyears[[yy]],"_",spnames[sp],"_det2_LandscapePreds.pdf")
    pdf(file=fileN, width=11,height=10)
    
    par(mfrow=c(4,4),mai=c(0.1,0.1,0.1,0.1))
    
    #Set the colour ramp 
    breaks<-seq(0,1,length.out=100)
    cols<-rev(terrain.colors(99))
    labs<-seq(0,1,by=0.1)
    
    for(i in 1:14){
      plot(occ.st[[i]],axes=FALSE, box=FALSE, legend=FALSE, main=years[i], breaks=breaks,col=cols)
      points(moddat[moddat$year==years[i] & moddat$collDet==1,],pch=20, cex=0.7)
    }
    
    #plot 2 "white" maps - only way I could find to move legend over!   
    plot(occ.st[[14]],axes=FALSE, box=FALSE, legend=FALSE, main=NULL, breaks=breaks,col=rep("white",99))
    plot(occ.st[[14]],axes=FALSE, box=FALSE, legend=FALSE, main=NULL, breaks=breaks,col=rep("white",99))
    
    #plot legend in position
    par(mar = c(0.2,0.2,0.2,7))
    plot(occ.st[[14]], legend.only=TRUE, col=cols,breaks = breaks,legend.width=1, legend.shrink=0.75,
         axis.args=list(at=seq(0,1,0.25),
                        labels=seq(0,1,0.25),
                        cex.axis=0.8),
         legend.args=list(text='Prob.Occ', side=3,font=2, line=0.5,cex=0.8))
    
    dev.off()
  }
}