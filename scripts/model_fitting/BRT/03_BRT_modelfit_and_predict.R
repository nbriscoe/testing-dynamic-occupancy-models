#################################################################
############# 03: FIT & PREDICT BRT MODELS  ######################
#################################################################

## This script fits BRTs for all 69 species using both the collDet dataset (collapsed data across all surveys)
## and the det2 dataset (middle survey only). 

## Outputs:
# raster predictions for each species > BRT/data_yrs/speciesName

# + summary results file with cv AUC, %dev explained, % variable contribution
# + saved model objects (RData)

load("data/occdyn_vars_AUG.RData") #Load species & env data

library(raster)

years <- 2000:2013
allvars <- stack(bio4.st, bio5.st, bio12.st, bio14.st, bio15.st, landuse.st, sd_ndvi.st)

var_year <- list()

for (i in seq_len(length(years))){
  var_year[[i]] <- c(paste0("bio4_",years[i]),
                     paste0("bio5_",years[i]),
                     paste0("bio12_",years[i]),
                     paste0("bio14_",years[i]),
                     paste0("bio15_",years[i]),
                     names(landuse.st),
                     paste0("sd_ndvi_",years[i]))
}
names(var_year) <- years

#how to make the site selector for the fold.vector argument in BRTs

sites <- order(unique(dets_covs_SDMs_NAs$siteID)) #267
temp <- sample(1:267,267)  #this makes it random
folds <- data.frame(temp, temp)
names(folds) <- c("site", "fold")
folds[,2] <- c(c(rep(1:10, each=26), 10), 1:6)
test <- match(dets_covs_SDMs_NAs$siteID, folds[,1])
dets_covs_SDMs_NAs$selector <- folds[test, 2]

library(dismo)
library(gbm)
names(dets_covs_SDMs_NAs) #predictors are columns 10 to 23; species detection is col 8 or 9

#species ids as numbers
species <- unique(dets_covs_SDMs_NAs$specid)

#species ids as names
spnames <- dets_covs_SDMs_NAs$CommonName[match(species, dets_covs_SDMs_NAs$specid)]

trainyears <- c("Data0002", "Data0004", "Data0009")
for(i in 1:length(trainyears)){
  dir.create(paste0("July2020/results/BRT/",trainyears[i]))
}

#if want to delete directories
# for(i in 1:length(trainyears)){
#   unlink(trainyears[i], recursive=T)
# }

for(k in 1:length(trainyears)){
  for(i in 1:length(spnames)){
    dir.create(file.path(paste0("July2020/results/BRT/",trainyears[k],"/",spnames[i])))
  }
}


#set up a list of filepaths
plot.filepaths <- list()
for(i in 1:length(trainyears)){
  plot.filepaths[[i]] <- list()
}
for(i in 1:length(trainyears)){
  for(k in 1:length(spnames)){
    plot.filepaths[[i]][[k]] <- file.path(paste0("July2020/results/BRT/",trainyears[i],"/", spnames[k]))
  }
}

cutoff <- c("2003", "2005", "2010") #the year after the end trainyear, for subsetting data

#having looked at amount of presence vs absence records per species for trainyear1, initial guesses for lr's and tc's for BRTs

lr <- c(0.002,0.002,0.0005,0.002,0.002,0.002,0.005,0.002,0.002,0.002,
        0.002,0.005,0.002,0.002,0.002,0.002,0.002,0.002,0.005,0.005,
        0.002,0.002,0.005,0.002,0.002,0.002,0.002,0.002,0.002,0.002,
        0.002,0.005,0.002,0.002,0.002,0.005,0.002,0.002,0.002,0.005,
        0.002,0.002,0.002,0.002,0.002,0.005,0.005,0.002,0.002,0.002,
        rep(0.002,10),
        0.002,0.002,0.002,0.002,0.002,0.002,0.002,0.002,0.005)

tc <- c(2,2,1,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,1,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2)

#Start timing
ptm <- proc.time() # Start timing
print(Sys.time())
#set up empty data frames for results, and a list to store all models.
for(yy in 1:3)
{
print(yy)
print(Sys.time())
dat_det2_res <- data.frame(matrix(NA, nrow=length(species), ncol=20))
names(dat_det2_res) <- c("nrow_data", "lr", "tc","trees","testAUC", "testDevExp", names(dets_covs_SDMs_NAs)[10:23])

dat_det2_preds <- data.frame(matrix(NA, nrow=nrow(dets_covs_SDMs_NAs), ncol=length(species)))
names(dat_det2_preds) <- paste0(spnames, "_det2")

det2_mods <- list()

#fit the models, make predictions to the full det_covs_SDMs file, and to rasters; save models
for(i in 1:length(species))
{

thissp <- species[i]
cat("this is species", i, "...",spnames[i], "\n")
moddat.thissp <- dets_covs_SDMs_NAs[dets_covs_SDMs_NAs$specid==thissp,]  

#do det2 first: the middle detection within the year
moddat <- moddat.thissp[,-9] 
moddat <- subset(moddat, moddat$year<cutoff[yy])
moddat <- na.omit(moddat)

temp <-  gbm.step(data=moddat, 
                  gbm.x = 9:22,
                  gbm.y = 8,
                  fold.vector = moddat$selector,
                  family = "bernoulli",
                  tree.complexity = tc[i],
                  learning.rate = lr[i],
                  bag.fraction = 0.75)

dat_det2_res[i,1] <- nrow(moddat)
dat_det2_res[i,2] <- lr[i]
dat_det2_res[i,3] <- tc[i]
dat_det2_res[i,4] <- temp$gbm.call$best.trees
dat_det2_res[i,5] <- temp$cv.statistics$discrimination.mean
dat_det2_res[i,6] <- 100* (temp$self.statistics$mean.null - temp$cv.statistics$deviance.mean) / temp$self.statistics$mean.null
matchem <- match(temp$contributions$var, names(dat_det2_res))
dat_det2_res[i,matchem] <- temp$contributions$rel.inf

pdf(file = file.path(plot.filepaths[[yy]][[i]], "/det2_BRT.pdf"), width=6.5, height=6.5)
par(mar=c(4,1,1,1))
gbm.plot(temp, plot.layout = c(4,4), y.label = "", write.title = F)  
dev.off()

dat_det2_preds[,i] <- predict(temp, newdata = dets_covs_SDMs_NAs, type = "response", n.trees = temp$gbm.call$best.trees)

for (zz in 1:length(years)){
year.rasters <- subset(allvars, subset=var_year[[zz]])
names(year.rasters) <- c("bio4","bio5","bio12","bio14","bio15","LC5","LC6","LC8","LC9","LC10","LC11","LC12","LC13_17","sd_ndvi")
pred2raster <- predict(year.rasters, temp,type = "response", n.trees = temp$gbm.call$best.trees, filename = paste0(plot.filepaths[[yy]][[i]], "/",trainyears[yy], "_",spnames[i],"_det2_BRT_", years[zz], ".tif"), format="GTiff")
}

det2_mods[[i]] <- temp

}
write.csv(dat_det2_res, file=paste0("July2020/results/BRT/",trainyears[yy],"/",trainyears[yy], "_det2_BRT_res.csv"))
write.csv(dat_det2_preds, file=paste0("July2020/results/BRT/",trainyears[yy],"/",trainyears[yy], "_det2_BRT_preds.csv"))
save(det2_mods, file = paste0("July2020/results/BRT/",trainyears[yy],"/",trainyears[yy], "_det2_BRT_models.RData"))
}
message(paste0('runtime ', (proc.time() - ptm)[3], ' seconds')) # Stop the clock

#########   

#now the collDet data

#Start timing
ptm <- proc.time() # Start timing

for(yy in 1:3)
  
{
  
  dat_collDet_res <- data.frame(matrix(NA, nrow=69, ncol=20))
  names(dat_collDet_res) <- c("nrow_data", "lr", "tc","trees","testAUC", "testDevExp", names(dets_covs_SDMs_NAs)[10:23])
  
  dat_collDet_preds <- data.frame(matrix(NA, nrow=nrow(dets_covs_SDMs_NAs), ncol=69))
  names(dat_collDet_preds) <- paste0(spnames, "_collDet")
  
  collDet_mods <- list()
  
  #for(i in 1:length(species))
  for(i in 1:69)
  {
    
    thissp <- species[i]
    cat("this is species", i, "...",spnames[i], "\n")
    moddat.thissp <- dets_covs_SDMs_NAs[dets_covs_SDMs_NAs$specid==thissp,]  
    
    #do det2: the middle detection within the year
    moddat <- moddat.thissp[,-8] 
    moddat <- subset(moddat, moddat$year<cutoff[yy])
    moddat <- na.omit(moddat)
    
    temp <-  gbm.step(data=moddat, 
                      gbm.x = 9:22,
                      gbm.y = 8,
                      fold.vector = moddat$selector,
                      family = "bernoulli",
                      tree.complexity = tc[i],
                      learning.rate = lr[i],
                      bag.fraction = 0.75)
    
    dat_collDet_res[i,1] <- nrow(moddat)
    dat_collDet_res[i,2] <- lr[i]
    dat_collDet_res[i,3] <- tc[i]
    dat_collDet_res[i,4] <- temp$gbm.call$best.trees
    dat_collDet_res[i,5] <- temp$cv.statistics$discrimination.mean
    dat_collDet_res[i,6] <- 100* (temp$self.statistics$mean.null - temp$cv.statistics$deviance.mean) / temp$self.statistics$mean.null
    matchem <- match(temp$contributions$var, names(dat_collDet_res))
    dat_collDet_res[i,matchem] <- temp$contributions$rel.inf
    
    pdf(file = file.path(plot.filepaths[[yy]][[i]], "/collDet_BRT.pdf"), width=6.5, height=6.5)
    par(mar=c(4,1,1,1))
    gbm.plot(temp, plot.layout = c(4,4), y.label = "", write.title = F)  
    dev.off()
    
    dat_collDet_preds[,i] <- predict(temp, newdata = dets_covs_SDMs_NAs, type = "response", n.trees = temp$gbm.call$best.trees)
    
    for (zz in 1:length(years)){
      year.rasters <- subset(allvars, subset=var_year[[zz]])
      names(year.rasters) <- c("bio4","bio5","bio12","bio14","bio15","LC5","LC6","LC8","LC9","LC10","LC11","LC12","LC13_17","sd_ndvi")
      pred2raster <- predict(year.rasters, temp,type = "response", n.trees = temp$gbm.call$best.trees, filename = paste0(plot.filepaths[[yy]][[i]], "/",trainyears[yy], "_",spnames[i],"_collDet_BRT_", years[zz], ".tif"), format="GTiff")
    }
    
    collDet_mods[[i]] <- temp
    
  }
  write.csv(dat_collDet_res, file=paste0("July2020/results/BRT/",trainyears[yy],"/",trainyears[yy], "_collDet_BRT_res.csv"))
  write.csv(dat_collDet_preds, file=paste0("July2020/results/BRT/",trainyears[yy],"/",trainyears[yy], "_collDet_BRT_preds.csv"))
  save(collDet_mods, file = paste0("July2020/results/BRT/",trainyears[yy],"/",trainyears[yy], "_collDet_BRT_models.RData"))
}
                
message(paste0('runtime ', (proc.time() - ptm)[3], ' seconds')) # Stop the clock

##CollDet = runtime runtime 15591.599 seconds (~4.5h for all)
#det2 similar. 

##Update format of rasters for consistency with other outputs
spp<-unique(detGGA$CommonName)
ls.data_yrs<-c("Data0002","Data0004","Data0009")
method<-"BRT"
datas<-c("collDet","det2")
for(sp in spp[35:69]){
  for(data.yrs in ls.data_yrs){
    for(d in datas){
    ls.rasters<-list.files(path=paste0("July2020/results/",method,"/",data.yrs,"/",sp),pattern=paste0(sp,"_",d,"_",method), full=TRUE)
    pred.st<-raster::stack(ls.rasters)
    writeRaster(pred.st,paste0("July2020/results/",method,"/",data.yrs,"/",sp,"/",data.yrs,"_",sp,"_",d,"_",method,".grd"))                       
    file.remove(ls.rasters)
    }
  }
}
